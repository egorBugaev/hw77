import {FETCH_GET_SUCCESS, FETCH_POST_SUCCESS} from "./actionTypes";
import axios from '../axios-api';

export const fetchPostSuccess = data => {
	return {type: FETCH_POST_SUCCESS, data};
};

export const fetchGetSuccess = messages => {
	return {type: FETCH_GET_SUCCESS, messages};
};

export const send = (data) => {
	return dispatch => {
		return axios.post('/messages', data).then(
			response => dispatch(fetchPostSuccess(response.data))
		);
	};
};
export const get = () => {
	return dispatch => {
		return axios.get('/messages/all',).then(
			response => dispatch(fetchGetSuccess(response.data))
		);
	};
};
