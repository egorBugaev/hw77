import React, {Component} from 'react';
import {connect} from "react-redux";
import './App.css';
import {get, send} from "./store/actions";


class App extends Component {
	componentDidMount() {
		this.props.getAll();
	}
	
	state = {
		message: '',
		author: '',
		image: null,
		preview: null
	};
	
	inputChangeHandler = event => {
		event.persist();
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	fileChangeHandler = event => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			
			reader.onload = (event) => {
				this.setState({preview: event.target.result});
			};
			
			reader.readAsDataURL(event.target.files[0]);
		}
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	sendMessageHandler = (e) => {
		e.preventDefault();
		const formData = new FormData();
		Object.keys(this.state).forEach(key => {
			if(key !== 'preview') formData.append(key,this.state[key]);
		});
		this.props.send(formData);
		this.setState({
			message: '',
			author: '',
			image: null,
			preview: null
		});
	};
	
	render() {
		return (
			<div className="container">
				<ul id="list">
					{this.props.messages.map(message => (
						<li key={message.id}>
							{message.image !== 'null' && <img src={'http://localhost:8000/uploads/' + message.image} alt={message.image}/>}
							<div className="right_message">
								<p className="author"><b>Author:</b> {message.author}</p>
								<p className="message"><b>Message:</b> {message.message}</p>
								<p className="date"><b>Date:</b> {message.date}</p>
							</div>
						</li>
					))}
				</ul>
				<div className="send-block">
					<form onSubmit={this.sendMessageHandler}>
						<label htmlFor="author">Автор:</label>
						<input id="author" type="text"
						       value={this.state.author} onChange={this.inputChangeHandler} name='author'
						       placeholder="Your name:"/>
						{this.state.preview && <img className="previewImg" src={this.state.preview} alt="previewImg"/>}
						<input type="file" name="image" onChange={this.fileChangeHandler}/>
						<label htmlFor="message">Сообщение:</label>
						<textarea required id="message" style={this.props.error ? {backgroundColor: 'red'} : null}
						          value={this.state.message}
						          onChange={this.inputChangeHandler} name='message'
						          placeholder="Your message:"/>
						<button className="send">Send</button>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		messages: state.messages,
		error: state.error
	};
};

const mapDispatchToProps = dispatch => {
	return {
		send: (data) => dispatch(send(data)),
		getAll: () => dispatch(get())
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);