const express = require('express');
const multer =require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');

const router = express.Router();

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload =multer({storage});


const createRouter = (db) => {
	
	router.get('/all', (req, res) => {
		res.send(db.getData());
	});
	
	router.post('/', upload.single('image'), (req, res) => {
		const message = req.body;
		if (req.file) {
			message.image = req.file.filename;
		}
		if (!message.author || message.author === ''){
			message.author = 'Anonymous'
		}
		if (!message.message || message.message === '') {
			res.send({error: "message must be"});
			res.status(400);
		} else {
			db.addItem(message).then(result => {
				res.status(200);
				res.send(result);
			});
		}
	});
	
	
	return router;
};

module.exports = createRouter;